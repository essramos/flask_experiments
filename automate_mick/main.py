"""
highlight row excel
https://stackoverflow.com/questions/48527243/format-entire-row-with-conditional-format-using-pandas-xlswriter-module

work on registration number not being string type
"""

import pandas as pd
import xlrd
import xlsxwriter
import time
from config import Config
from qbcc import process_qbcc
from boaq_architect import process_architects
from bpeq_engineers import process_engineers
from pool_inspector import process_pool_inspector

def get_excel_rows(dataframe):
    dataframe = dataframe.replace(pd.np.nan, '', regex=True)
    columns = list(dataframe.columns)
    if "Notes" not in columns:
        columns.insert(len(columns), "Notes")
    values = dataframe.values.tolist()
    return columns, values


def process_excel_rows(columns, values, url, process): 
    for row in values:
        if len(row) < len(columns):
            row.append("")
        print ("old: \n", row)
        if process == "qbcc":
            try:          
                process_qbcc(row, columns, url)
                print ("new: \n", row)
            except Exception as e:
                print (e.args)
                pass    
        elif process == "boaq":
            process_architects(row, columns, url)
            print ("new: \n", row)
        elif process == "bpeq":
            process_engineers(row, columns, url)
            print ("new: \n", row)
        elif process == "myqbcc":
            process_pool_inspector(row, columns, url)
            print ("new: \n", row)
            time.sleep(2)
    return values


def convert_to_df(columns, vals):
    """
    return dictionary of dataframe
    """
    dataframe = {}
    for row in vals:
        for ind, val in enumerate(row):
            if columns[ind] not in dataframe:
                dataframe[columns[ind]] = []
            dataframe[columns[ind]].append(val)
    df = pd.DataFrame(dataframe)

    return df


def save_to_excel(dataframes, column_mapping):
    """
    overwrite excel sheet with new data
    """
    writer = pd.ExcelWriter('new.xlsx', engine='xlsxwriter')
    for sheetname, df in dataframes.items():  # loop through `dict` of dataframes
        print ("saving sheetname: ", sheetname)
        columns = column_mapping[sheetname]
        print columns
        df.to_excel(writer, sheet_name=sheetname, columns=columns, index=False)  # send df to writer
        worksheet = writer.sheets[sheetname]  # pull worksheet object

        number_rows = len(df.index) + 1
        workbook = writer.book
        col_to_name = xlsxwriter.utility.xl_col_to_name(columns.index("Status"))
        last_col_to_name = xlsxwriter.utility.xl_col_to_name(len(columns)-1)

        format1 = workbook.add_format(
            {
                'bg_color': '#FFC7CE',
                'font_color': '#9C0006'
            }
        )
        wrap_text = workbook.add_format(
            {'text_wrap': True}
        )

        format2 = workbook.add_format(
            {
                'bg_color': '#ffff00',
                'font_color': '#9C0006'
            }
        )

        worksheet.conditional_format(
            "$A$1:${}${}".format(last_col_to_name, number_rows),
            {
                "type": "formula",
                "criteria": '=INDIRECT("{}"&ROW())="NO RECORD"'.format(col_to_name),
                "format": format2
            }
        )

        worksheet.conditional_format(
           "$A$1:${}${}".format(last_col_to_name, number_rows),
            {
                "type": "formula",
                "criteria": '=INDIRECT("{}"&ROW())="NOT ACTIVE"'.format(col_to_name),
                "format": format1
            }
        )

        for idx, col in enumerate(df):  # loop through all columns
            series = df[columns[idx]]
            max_len = max((
                series.astype(str, errors='ignore').map(len).max(),  # len of largest item
                len(str(series.name))  # len of column name/header
                )) + 1  # adding a little extra space
            worksheet.set_column(idx, idx, max_len, wrap_text)  # set column width
        worksheet.freeze_panes(1, 0)
    writer.save()


if __name__ == "__main__":
    # columns, values = get_excel_rows()
    # processed_vals = process_excel_rows(columns, values)
    # save_to_excel(columns, processed_vals)

    links = Config.link_mapping

    xl_file = pd.ExcelFile('test_new.xlsx')

    dfs = {sheet_name: xl_file.parse(sheet_name, converters={'Registration':str}) 
          for sheet_name in xl_file.sheet_names if sheet_name in links}

    dataframes = {}
    column_mapping = {}
    for sheet, link in links.iteritems():
        dataframe = dfs[sheet]
        columns, values = get_excel_rows(dataframe)
        link, process = link
        print (sheet)
        print (columns)
        column_mapping[sheet] = columns
        try:
            processed_vals = process_excel_rows(columns, values, link, process)
            dataframes[sheet] = convert_to_df(columns, processed_vals)
        except Exception as e:
            print ("Error with this sheet: ", sheet)
            print (e.args)
            # original values
            dataframe[sheet] = convert_to_df(columns, values)

    print (dataframes)
    save_to_excel(dataframes, column_mapping)