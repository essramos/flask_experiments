"""
highlight row excel
https://stackoverflow.com/questions/48527243/format-entire-row-with-conditional-format-using-pandas-xlswriter-module
"""

import requests
import time
import pandas as pd
import datetime
import json
from bs4 import BeautifulSoup

licenses = ["23010", "709132"]


def get_excel_rows():
    file = "test2.xlsx"
    df = pd.read_excel(file, 'Builder')
    df = df.replace(pd.np.nan, '', regex=True)

    columns = list(df.columns)
    if "Notes" not in columns:
        columns.insert(8, "Notes")
    values = df.values.tolist()
    return columns, values


def process_excel_rows(columns, values):  
    for row in values:
        if len(row) < 9:
            row.append("")
        active_flag = 0
        surname = row[0] if row[0] != "" else None
        firstname = row[1] if row[1] != "" else None  # get only first name, ignore second name
        license_num = str(row[2]) if row[2] != "" else None
        business_name = row[3] if row[3] != "" else None # compare this if name isn't complete
        url = "http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?LicNO={}&name=&firstName=&licCat=LIC&searchType=Contractor".format(license_num)
        print url
        result = requests.get(url)
        soup = BeautifulSoup(result.text, features="html.parser")
        # check if active or not 
        table = soup.find('table', attrs={'id': 'ctl00_generalContentPlaceHolder_LicenceInfoControl1_gvLicenceClass'})

        if table:
            rows = table.find_all('tr')
            all_rows = []
            for t_row in rows:
                cols = t_row.find_all('td')
                cols = [ele.text.strip() for ele in cols]
                all_rows.append(cols)
            if len(all_rows[0]) == 0:
                all_rows.pop(0)
            if len(all_rows) > 1:
                row[8] = ""
                for col in all_rows:
                    if 'active' not in col[-1].lower():
                        if row[8] == "":
                            row[8] = "{} - {}".format(col[0], col[-1])
                        else:
                            row[8] = "{}, {} - {}".format(row[8], col[0], col[-1])
                        
            else:
                print "one row check if suspended"
                if 'active' not in all_rows[0][-1].lower():
                    active_flag = 1
                    row[8] = "{} - {}".format(all_rows[0][0], all_rows[0][-1])
        else:
            active_flag = 1
      
        license_from_web = soup.find("span", {
            "id": "ctl00_generalContentPlaceHolder_LicenceInfoControl1_lbLicenceNO"
            }
        ).text.strip()

        license_name_from_web = soup.find("span", {
            "id": "ctl00_generalContentPlaceHolder_LicenceInfoControl1_lbLicenceName"
            }
        ).text.strip()
        print "{}, {}".format(surname, firstname)
        print "business name from excel: ", business_name
        print "business name from web: ", license_name_from_web
        if surname is not None and firstname is not None:
            print "complete name present"
            complete_name = "{}, {}".format(surname, firstname)
            if license_name_from_web.lower() != complete_name.lower():
                print "{} != {}".format(license_name_from_web.lower(), complete_name.lower())
                if business_name is not None:
                    if license_name_from_web.lower() != business_name.lower():
                        print "{} != {}".format(license_name_from_web.lower(), business_name.lower())
                        row[3] = license_name_from_web
                else:
                    row[3] = license_name_from_web
                    
        elif surname is not None and firstname is None:
            print "only surname is present"
            if surname != license_name_from_web:
                if license_name_from_web.lower() != business_name.lower():
                    print "{} != {}".format(license_name_from_web.lower(), business_name.lower())
                    row[3] = license_name_from_web
        elif business_name is not None:
            print "only business name is present"
            if business_name != license_name_from_web:
                row[3] = license_name_from_web

        if active_flag == 0:
            row[6] = "ACTIVE"
        elif active_flag == 1:
            row[6] = "NOT ACTIVE"

        row[7] = datetime.datetime.utcnow().isoformat()

        print row
        print columns
        time.sleep(5)
    return values


def save_to_excel(columns, vals):
    """
    overwrite excel sheet with new data
    """

    dataframe = {}
    for row in vals:
        for ind, val in enumerate(row):
            if columns[ind] not in dataframe:
                dataframe[columns[ind]] = []
            dataframe[columns[ind]].append(val)
    print vals
    print json.dumps(dataframe)
    df = pd.DataFrame(dataframe)

    with pd.ExcelWriter("test2.xlsx", engine='xlsxwriter') as writer:
        df.to_excel(writer, sheet_name="Builder", columns=columns, index=False)
        df.reset_index()

        number_rows = len(df.index) + 1

        workbook = writer.book
        worksheet = writer.sheets['Builder']

        format1 = workbook.add_format(
            {'bg_color': '#FFC7CE',
            'font_color': '#9C0006'}
        )
        wrap_text = workbook.add_format(
            {'text_wrap': True}
        )

        worksheet.conditional_format(
            "$A$1:$I$%d" % (number_rows),
            {
                "type": "formula",
                "criteria": '=INDIRECT("G"&ROW())="NOT ACTIVE"',
                "format": format1            
            }
        )

        worksheet.set_column(0, 0, 20)
        worksheet.set_column(1, 1, 10)
        worksheet.set_column(2, 2, 15)
        worksheet.set_column(3, 3, 40, wrap_text)
        worksheet.set_column(4, 4, 40, wrap_text)
        worksheet.set_column(5, 5, 40, wrap_text)
        worksheet.set_column(6, 6, 15)
        worksheet.set_column(7, 7, 30, wrap_text)
        worksheet.set_column(8, 8, 40, wrap_text)
        worksheet.freeze_panes(1, 0)
        writer.save()


if __name__ == "__main__":
    columns, values = get_excel_rows()
    processed_vals = process_excel_rows(columns, values)
    save_to_excel(columns, processed_vals)

    # vals = [
    #     [u'1Struct', '', 1137930L, u'Innovative Wealth Creation Pty Ltd', u'Builder - Lowrise, Carpentry', u'No', 'ACTIVE', datetime.datetime.utcnow().isoformat(), ""],
    #     [u'Daley', u'Mark', 15023860L, u'Daley Built Pty. Ltd.', u'Builder - Open', u'Yes - restricted to domestic building construiction only', 'NOT ACTIVE', datetime.datetime.utcnow().isoformat(), 'Builder - Open - SUSPEND-ROLL FEE NOT PAID'],
    #     [u'Acimovic', u'Milan', 1307784L, u'Constructright Pty Ltd', u'Builder - Lowrise', u'No', 'ACTIVE', datetime.datetime.utcnow().isoformat(), ""],
    #     [u'Bosfield Group', '', 1285544L, u'Bosfield Group Pty Ltd', u'Builder Low Rise', u'NO', 'NOT ACTIVE', datetime.datetime.utcnow().isoformat(), ""]
    # ]