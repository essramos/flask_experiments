import requests
import datetime
import time
from bs4 import BeautifulSoup


def process_qbcc(row, columns, url):
    surname_index = columns.index("Surname")
    firstname_index = columns.index("First name")
    license_index = columns.index("Registration")
    business_name_index = columns.index("Business Name")
    status_index = columns.index("Status")
    date_index = columns.index("Last Checked")
    notes_index = columns.index("Notes")

    active_flag = 0
    surname = row[surname_index] if row[surname_index] != "" else None
    firstname = row[firstname_index] if row[firstname_index] != "" else None  # get only first name, ignore second name
    license_num = str(row[license_index]) if row[license_index] != "" else None
    business_name = row[business_name_index] if row[business_name_index] != "" else None # compare this if name isn't complete


    if not license_num:
        row[notes_index] = "Registration Number is missing"
        row[status_index] = "NO RECORD"
        row[date_index] = datetime.datetime.utcnow().isoformat()
        return

    url = url.replace("replace_me", license_num)
    print ("URL: ", url)

    result = requests.get(url)
    if result.status_code != 200:
        row[status_index] = "NO RECORD"
        row[date_index] = datetime.datetime.utcnow().isoformat()
        return

    soup = BeautifulSoup(result.text, features="html.parser")
    not_found_lbl = soup.find("span", {
        "id": "ctl00_generalContentPlaceHolder_SearchSumResultsControl1_lbMessage"
        }
    )

    if not_found_lbl is not None:
        row[status_index] = "NO RECORD"
        row[date_index] = datetime.datetime.utcnow().isoformat()
        return

    # not found label ctl00_generalContentPlaceHolder_SearchSumResultsControl1_lbMessage
    # check if active or not 
    table = soup.find('table', attrs={'id': 'ctl00_generalContentPlaceHolder_LicenceInfoControl1_gvLicenceClass'})

    if table:
        rows = table.find_all('tr')
        all_rows = []
        for t_row in rows:
            cols = t_row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            all_rows.append(cols)
        if len(all_rows[0]) == 0:
            all_rows.pop(0)
        if len(all_rows) > 1:
            row[notes_index] = ""
            for col in all_rows:
                if 'active' not in col[-1].lower():
                    if row[notes_index] == "":
                        row[notes_index] = "{} - {}".format(col[0], col[-1])
                    else:
                        row[notes_index] = "{}, {} - {}".format(row[notes_index], col[0], col[-1])
                    
        else:
            print ("one row check if suspended")
            if 'active' not in all_rows[0][-1].lower():
                active_flag = 1
                row[notes_index] = "{} - {}".format(all_rows[0][0], all_rows[0][-1])
    else:
        active_flag = 1

    license_name_from_web = soup.find("span", {
        "id": "ctl00_generalContentPlaceHolder_LicenceInfoControl1_lbLicenceName"
        }
    ).text.strip()

    print ("{}, {}".format(surname, firstname))
    print ("business name from excel: ", business_name)
    print ("business name from web: ", license_name_from_web)
    # if surname is not None and firstname is not None:
    #     print ("complete name present")
    #     complete_name = "{}, {}".format(surname, firstname)
    #     if license_name_from_web.lower() != complete_name.lower():
    #         print ("{} != {}".format(license_name_from_web.lower(), complete_name.lower()))
    #         if business_name is not None:
    #             if license_name_from_web.lower() != business_name.lower():
    #                 print ("{} != {}".format(license_name_from_web.lower(), business_name.lower()))
    #                 row[business_name_index] = license_name_from_web
    #         else:
    #             row[business_name_index] = license_name_from_web
                
    # elif surname is not None and firstname is None:
    #     print ("only surname is present")
    #     if surname != license_name_from_web:
    #         if business_name is not None:
    #             if license_name_from_web.lower() != business_name.lower():
    #                 print ("{} != {}".format(license_name_from_web.lower(), business_name.lower()))
    #                 row[business_name_index] = license_name_from_web
    #         else:
    #             row[business_name_index] = license_name_from_web
    # elif business_name is not None:
    #     print ("only business name is present")
    #     if business_name.lower() != license_name_from_web.lower():
    #         row[business_name_index] = license_name_from_web
    # else:
    row[business_name_index] = license_name_from_web

    if active_flag == 0:
        row[status_index] = "ACTIVE"
    elif active_flag == 1:
        row[status_index] = "NOT ACTIVE"

    row[date_index] = datetime.datetime.utcnow().isoformat()

    time.sleep(1.5)