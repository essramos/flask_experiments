import os
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import requests
import datetime
from bs4 import BeautifulSoup


def validate_element(soup, id):
    try:
        element = soup.find("span", {
            "id": id
        }).text.strip()
    except Exception as e:
        print e.args
        element = None

    return element


def process_architects(row, columns, url):
    business_name_index = columns.index("Business Name")
    status_index = columns.index("Status")
    date_index = columns.index("Last Checked")
    registered_to_index = columns.index("Registered To")
    level_index = columns.index("level")
    license_index = columns.index("Registration")
    notes_index = columns.index("Notes")

    license_num = str(row[license_index]) if row[license_index] != "" else None
    row[date_index] = datetime.datetime.utcnow().isoformat()
    if license_num is not None:
        curr_dir = os.getcwd()
        driver_path = os.path.join(curr_dir, "chromedriver")
        driver = webdriver.Chrome(executable_path=driver_path)

        driver.get("https://www.boaq.qld.gov.au/BOsAQ/Search_Register/BOAQ/Search_Register/Architect_Search.aspx?hkey=b057ad1c-2fc3-4b3b-9cc6-34d238b01569")

        inputElement = driver.find_element_by_id("ctl01_TemplateBody_WebPartManager1_gwpciArchitectsearch_ciArchitectsearch_ResultsGrid_Sheet0_Input3_TextBox1")
        inputElement.send_keys(license_num)              
        driver.find_element_by_name("ctl01$TemplateBody$WebPartManager1$gwpciArchitectsearch$ciArchitectsearch$ResultsGrid$Sheet0$SubmitButton").click()

        try:
            t = wait(driver, 15).until(
                EC.presence_of_element_located((
                    By.ID,
                    "ctl01_TemplateBody_WebPartManager1_gwpciArchitectsearch_ciArchitectsearch_ResultsGrid_Grid1_ctl00__0")
                    )
            )
            print t
        except Exception as e:
            print e.args
            row[status_index] = "NO RECORD"
            return

        links = [link.get_attribute('href') for link in t.find_elements_by_tag_name('a')]
        result = requests.get(links[0])
        if result.status_code != 200:
            row[status_index] = "NO RECORD"
            return

        result.encoding = 'utf-8'
        soup = BeautifulSoup(result.text, features="html.parser")

        status = validate_element(soup, "ctl01_TemplateBody_WebPartManager1_gwpciProfile_ciProfile_contactStatus_memberStatus")
        company = validate_element(soup, "ctl01_TemplateBody_WebPartManager1_gwpciProfileSection_ciProfileSection_CsContact.Company")                                
        registered_to = validate_element(soup, "ctl01_TemplateBody_WebPartManager1_gwpciProfile_ciProfile_contactStatus_paidThruDate")
        area = validate_element(soup, "ctl01_TemplateBody_WebPartManager1_gwpciProfile_ciProfile_contactStatus_memberType")

        row[business_name_index] = company
        row[registered_to_index] = registered_to
        row[level_index] = area
        if status is not None and status == 'Active':
            row[status_index] = 'ACTIVE'
        else:
            row[status_index] = 'NOT ACTIVE'

        driver.close()
    else:
        row[notes_index] = "Registration Number is missing"
        row[status_index] = "NO RECORD"
        driver.close()
        return