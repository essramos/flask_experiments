class Config:
    link_mapping = {
        # "Certifiers": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?LicNO=replace_me&licCat=ACCR&name=&firstName=&searchType=Certifier&FromPage=SearchContr", "qbcc"),
        # "Building Designers": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor&FromPage=SearchContr", "qbcc"),
        # "Commercial Register": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?&LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor", "qbcc"),
        # "Builder": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?LicNO=replace_me&name=&firstName=&licCat=LIC&searchType=Contractor", "qbcc"),
        # "Energy Eff F16": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?&LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor", "qbcc"),
        # "Fire Wall Inspectorsinstallers ": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?&LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor", "qbcc"),
        # "Fire Services F16": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?&LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor", "qbcc"),
        # "Termite F16": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?&LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor", "qbcc"),
        # "Glazing F15": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?&LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor", "qbcc"),
        # "Waterproofing F16": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?&LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor", "qbcc"),
        # "Architects": ("https://www.boaq.qld.gov.au/BOAQ/Search_Register/BOAQ/Search_Register/Architect_Search.aspx?hkey=b057ad1c-2fc3-4b3b-9cc6-34d238b01569", "boaq"),
        # "Engineers": ("http://www.bpeq.qld.gov.au/BPEQ/Search_for_a_RPEQ/Search_Register/BPEQ/Navigation/SearchRegister/Engineer_Search.aspx?hkey=0914fde4-8f61-4cf5-97a3-c477456e8e0f", "bpeq"),
        # "Miscellaneous": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor&FromPage=SearchContr", "qbcc"),
        # "Soil Testers RPEQ": ("https://www.bpeq.qld.gov.au/imis15/BPEQ/Finding_an_RPEQ/Search_Register/BPEQ/Navigation/SearchRegister/Directory.aspx?hkey=0914fde4-8f61-4cf5-97a3-c477456e8e0f", "bpeq"),
        # "Soil Testers BPEQ": ("http://www.onlineservices.qbcc.qld.gov.au/OnlineLicenceSearch/VisualElements/ShowDetailResultContent.aspx?LicNO=replace_me&licCat=LIC&name=&firstName=&searchType=Contractor&FromPage=SearchContr", "qbcc")
        "Pool Safety Inspector": ("https://my.qbcc.qld.gov.au/s/pool-safety-inspector-search", "myqbcc")
    }