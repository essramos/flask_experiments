from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
from selenium.webdriver.support.ui import WebDriverWait  as wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import requests
from bs4 import BeautifulSoup
from selenium.common.exceptions import NoSuchElementException
import sys
import time
import datetime
"""
NOTES: need a valid registration
"""
curr_dir = os.getcwd()


def process_pool_inspector(row, columns, url):
    
    business_name_index = columns.index("Business Name")
    status_index = columns.index("Status")
    date_index = columns.index("Last Checked")
    registered_to_index = columns.index("Registered To")
    license_index = columns.index("Registration")
    notes_index = columns.index("Notes")

    license_num = str(row[license_index]) if row[license_index] != "" else None
    row[date_index] = datetime.datetime.utcnow().isoformat()
    driver = webdriver.Chrome(executable_path=driver_path, chrome_options=options)

    if license_num is not None:
        driver_path = os.path.join(curr_dir, "chromedriver")
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--ignore-ssl-errors')
        driver.get(url)
        driver.find_element_by_xpath('//*[@id="28:2;a"]/option[5]').click()
        # driver.find_element_by_css_selector("#\\32 8\\3a 2\\3b a > option:nth-child(5)").click()
        inputElement = wait(driver, 15).until(
            EC.presence_of_element_located((
                By.ID,
                "input-1")
                )
        )

        inputElement.send_keys(license_num)
        driver.find_element_by_css_selector("#NapiliCommunityTemplate > div.cCenterPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div > div > div.slds-grid.search-bar.slds-p-bottom--medium.slds-p-left--medium.slds-p-right--medium.slds-wrap > div.slds-col.slds-small-size--1-of-1.slds-medium-size--3-of-4.slds-large-size--3-of-4 > div > div.slds-col.slds-small-size--1-of-1.slds-medium-size--1-of-3.slds-large-size--1-of-3.slds-p-left--small.btn-search > button").click()

        licenseElement = wait(driver, 15).until(
            EC.presence_of_element_located((
                By.XPATH,
                '//*[@id="status-div"]/p')
                )
        )

        if licenseElement.text.startswith("Sorry"):
            row[status_index] = "NO RECORD"
            return
        else:
            print "hey"
            try:
                driver.find_element_by_xpath('//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div/div/div[2]/div[3]/div[2]/div/div/button[1]').click()
                # wait for details of licensee
                map_element = wait(driver, 30).until(
                    EC.presence_of_element_located((
                        
                        By.XPATH,
                        '//*[@id="map_a1V9000000G6RITEA3"]/div[1]')
                        )
                )
                print map_element.text
                driver.find_element_by_xpath('//*[@id="map_a1V9000000G6SWgEAN"]/div[1]/div/div[2]/button').click()
                

                name = driver.find_element_by_xpath('//*[@id="map_a1V9000000G6T9TEAV"]/div[2]/div/div/div/div/div/div[1]/div/div[2]/div[1]/div[2]').text
                expiration = driver.find_element_by_xpath('//*[@id="map_a1V9000000G6T9TEAV"]/div[2]/div/div/div/div/div/div[2]/div/div[2]/div[3]/div[2]/span').text
                                                        
                print name, expiration
                row[business_name_index] = name
                row[registered_to_index] = expiration
                driver.close()
            except Exception as e:
                print e.args
    else:
        print "no license number"
        row[notes_index] = "Registration Number is missing"
        row[status_index] = "NO RECORD"
        driver.close()
        return